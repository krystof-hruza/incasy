import { Button, CircularProgress, Grid, Typography } from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import useShips from "../../../data/hooks/useShips";
import { getCountryfromOwner } from "../../../lib/services/country.service";
import { ErrorBlock } from "../../blocks/ErrorBlock";
import RecordsList from "../../blocks/RecordsList/RecordsList";

export function ShipDetail() {
  const { shipId } = useParams();
  const { data, isLoading, isError } = useShips();
  const parsedId = Number(shipId);
  const ship = data.ships.find((s) => s.id === parsedId);
  const navigate = useNavigate();

  if (isLoading) {
    return <CircularProgress />;
  } else if (isNaN(parsedId) || isError || !ship) {
    return <ErrorBlock />; //TODO: Provide reason for error
  } else {
    const records = [
      { label: "Country of origin", value: getCountryfromOwner(ship.owner) },
      { label: "Built", value: ship.built },
      { label: "Name", value: ship.name },
      { label: "Length (m)", value: ship.lengthMeters },
      { label: "Beam (m)", value: ship.beamMeters },
      { label: "TEU (max)", value: ship.maxTEU },
      { label: "Owner", value: ship.owner },
      { label: "Gross tonnage", value: ship.grossTonnage },
    ];
    return (
      <Grid container p={1}>
        <Grid item xs={10}>
          <Typography variant="h2">{ship?.name}</Typography>
        </Grid>
        <Grid
          item
          xs={2}
          display="flex"
          alignItems="center"
          justifyContent="flex-end"
        >
          <Button onClick={() => navigate("/")}>Go back</Button>
        </Grid>
        <Grid xs={12}>
          <RecordsList items={records} />
        </Grid>
      </Grid>
    );
  }
}
