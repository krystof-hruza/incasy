import { CircularProgress, Grid } from "@mui/material";
import useShips from "../../../data/hooks/useShips";
import { ShipType } from "../../../data/types/ShipType";
import { ErrorBlock } from "../../blocks/ErrorBlock";
import ShipCard from "../../blocks/ShipCard/ShipCard";

export function ShipList() {
  const { data, isLoading, isError } = useShips();

  const content = isLoading ? (
    <CircularProgress />
  ) : isError ? (
    <ErrorBlock />
  ) : (
    data.ships.map((ship: ShipType, i: number) => (
      <Grid item xs={12} lg={4} key={i}>
        <ShipCard ship={ship} />
      </Grid>
    ))
  );

  return (
    <Grid container spacing={2}>
      {content}
    </Grid>
  );
}
