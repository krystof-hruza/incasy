import { Typography } from "@mui/material";

export function ErrorBlock() {
  return <Typography>Something went wrong</Typography>;
}
