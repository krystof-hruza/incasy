import { Card, CardActionArea, CardContent, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { RecordRowType } from "../../../data/types/AppType";
import { ShipType } from "../../../data/types/ShipType";
import { getCountryfromOwner } from "../../../lib/services/country.service";
import RecordsList from "../RecordsList/RecordsList";

const ShipCard = ({ ship }: { ship: ShipType }) => {
  const country = getCountryfromOwner(ship.owner);
  const navigate = useNavigate();
  let records: RecordRowType[] = [
    { label: "Country of origin", value: country, hide: !country },
    { label: "TEU", value: ship.maxTEU },
  ];

  return (
    <Card>
      <CardActionArea onClick={() => navigate(`/detail/${ship.id}`)}>
        <CardContent>
          <Typography variant="h5">{ship.name}</Typography>
          <RecordsList items={records} />
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default ShipCard;
