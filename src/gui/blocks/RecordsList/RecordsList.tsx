import { Paper, Table, TableBody, TableContainer } from "@mui/material";
import { RecordRowType } from "../../../data/types/AppType";
import RecordRow from "../RecordRow/RecordRow";

const RecordsList = ({ items }: { items: RecordRowType[] }) => {
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableBody>
          {items
            .filter((item) => !item.hide)
            .map((item, index) => (
              <RecordRow item={item} key={index} />
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default RecordsList;
