import { TableCell, TableRow, Typography } from "@mui/material";
import { RecordRowType } from "../../../data/types/AppType";

const RecordRow = ({ item }: { item: RecordRowType }) => {
  return (
    <TableRow>
      <TableCell sx={{ border: "none" }}>
        <Typography component="strong" sx={{ fontWeight: "bold" }} mr={1}>
          {item.label}
        </Typography>
      </TableCell>
      <TableCell sx={{ border: "none" }}>
        <Typography component="span">{item.value}</Typography>
      </TableCell>
    </TableRow>
  );
};

export default RecordRow;
