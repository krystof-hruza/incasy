import { createBrowserRouter } from "react-router-dom";
import App from "./gui/app/App";
import { ShipDetail } from "./gui/views/ShipDetail";
import { ShipList } from "./gui/views/ShipList";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/",
        element: <ShipList />,
      },
      {
        path: "/detail/:shipId",
        element: <ShipDetail />,
      },
    ],
  },
]);
