import useSWR from "swr";
import apiFetcher from "../../lib/services/fetcher.service";
import { apiURLs } from "../constants/api";
import { ShipType } from "../types/ShipType";

export default function useShips() {
  const { data, error, isLoading } = useSWR(apiURLs.shipList, apiFetcher);
  const typeHintedData: { ships: ShipType[] } = data || { ships: [] };
  return {
    data: typeHintedData,
    isLoading,
    isError: error,
  };
}
