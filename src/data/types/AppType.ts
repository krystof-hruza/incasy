export interface RecordRowType {
  label: string
  value: string | number | null
  hide?: boolean
}