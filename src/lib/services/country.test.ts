import { getCountryfromOwner } from './country.service'

test('get country from owner with country', () => {
  expect(getCountryfromOwner('Owner name with a (Country name)')).toBe('Country name')
})

test('get undefined from owner without a country', () => {
  expect(getCountryfromOwner('Owner name without a country name')).toBeNull()
})

test('get country from owner with country in middle ', () => {
  expect(getCountryfromOwner('Owner name with a (Country name) in middle.')).toBe('Country name')
})