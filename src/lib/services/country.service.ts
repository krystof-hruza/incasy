/**
 * Extracts any substring located inside the first pair of brackets. It is assumed that that is a country name.
 * @param owner A string to be searched for a country name.
 * @returns Country name as a string or undefined if not found
 */
export const getCountryfromOwner = (owner: string) => {
  const countryWithBrackets = owner.match(/\((.*)\)/);
  return countryWithBrackets && countryWithBrackets[1] || null
};