export default function apiFetcher(url: string) {
  return fetch(url, {
    method: 'GET',
  })
    .then(res => res.text())
    .then(res => JSON.parse(res))
}