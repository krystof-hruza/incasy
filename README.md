# Great and useful application for a quick overview of the world's largest ships

This app has been created as an interview assignment for Inelligent Cargo Systems.

## How to run this app:

### How to run for developers:

- Clone the repo `git clone [repo adress]`
- Navigate to project folder `cd [local path to repo]`
- Run `yarn start` or `npm run start` to run the app locally. (for development)
- Run `yarn build` or `npm run build` to compile a production version of this app
- If you ran the `build` script, serve the content of _/build_ folder with your server solution of choice

### How to run for non developers:

#### Prerequisities:

First you have to install **NodeJS**. You can get it either at https://nodejs.org/en or via your package manager.

Then you will have to _clone_ this repository. If you don't know how to do that, don't worry, you can learn how to do that [here](https://docs.github.com/en/repositories/creating-and-managing-repositories/cloning-a-repository?platform=windows).

#### Running locally:

If you just want to run the app on your computer, open up your terminal (Command line on Windows). Now that your terminal is open, write following instructions into it:

**Navigate to your project**
`cd [replace this with the path to your project]`

**Run the app**
`npm run start`

Now a new browser tab should open with this application running inside it. Enjoy. :)

#### Running in production

Ask someone, who knows what they are doing, how one would go about compiling and deploying a production build or better yet, to do it for you. If you are brave and curious you can read a bit about it [here](https://rapidapi.com/blog/how-to-deploy-a-react-app/).

## Troubleshooting:

If you encounter any issues with Node version, the code is tested to work with Node 20.5.1. Try running that version of Node.
If you encounter any other issues you can reach me at krystof.hruza@skiff.com.
